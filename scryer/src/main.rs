use scryer_prolog::machine::Machine;
use scryer_prolog::machine::parsed_results::QueryResolution;
use scryer_prolog::machine::parsed_results::QueryMatch;
use scryer_prolog::machine::parsed_results::QueryResult;
use scryer_prolog::machine::parsed_results::Value;

use collection_macros::btreemap;

fn main() {
    let mut machine = Machine::new_lib();

    machine.load_module_string("facts", String::from(r#"
        triple("a", "p1", "b").
        triple("a", "p2", "b").
    "#));
    
    let output: QueryResult = machine.run_query(String::from(r#"triple("a",P,"b")."#));
    println!("P{:?}", output);
    assert_eq!(output, Ok(QueryResolution::Matches(vec![
         QueryMatch::from(btreemap! {
             "P" => Value::from("p1"),
         }),
         QueryMatch::from(btreemap!{
             "P" => Value::from("p2"),
         }),
    ])));
}
