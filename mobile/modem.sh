#!/bin/sh

# https://wiki.postmarketos.org/wiki/PINE64_PinePhone_(pine64-pinephone)#Modem
# https://wiki.pine64.org/wiki/PinePhone_APN_Settings#Free_(France)
sudo mmcli -m 1 --create-bearer='apn=free,user=free,password=,allowed-auth=chap,allow-roaming=no'
sudo nmcli c add type gsm ifname cdc-wdm0 con-name Free apn free user free password ''
nmcli c
nmcli d
