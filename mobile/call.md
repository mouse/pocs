% sudo mmcli -m 0 --voice-create-call="number='+33695501298'"
  --------------------------
  General      |       path: /org/freedesktop/ModemManager1/Call/0
  --------------------------
  Properties   |     number: +33695501298
               |  direction: outgoing
               | multiparty: no
  --------------------------
  Audio format |       rate: 0

% sudo mmcli -m 0 -o '/org/freedesktop/ModemManager1/Call/0' --start

successfully started the call

% sudo mmcli -m 0 -o '/org/freedesktop/ModemManager1/Call/0' --hangup
successfully hung up the call

% mmcli -m 0 --voice-delete-call "/org/freedesktop/ModemManager1/Call/0"
successfully deleted call from modem


callaudiod

journalctl | grep callaudiod



mmcli -m any --voice-list-calls
