defmodule MessageProcessor do
  def handle_messages(messages) do
    for message <- messages do
      value = Map.fetch!(message, :value)
      case Avrora.Encoder.decode_plain(value, schema_name: "Payment") do
        {:ok, decoded_message} ->
          IO.inspect(decoded_message, label: "Avro")
        _ ->
          IO.inspect(message, label: "not Avro")
      end
    end
    :ok
  end

  def handle_messages(), do: :ok
end

defmodule Hello do
  def send_default(), do: send_message("3", 3.3)
  def send_message(id, amount) do
    message = %{id: id, amount: amount}
    {:ok, encoded_message} = Avrora.Encoder.encode_plain(message, schema_name: "Payment")
    Kaffe.Producer.produce_sync("testtopic", encoded_message)
  end
end

defmodule Hello.Application do
  use Application
  def start(_type, _args) do
    children = [
      %{
        id: Kaffe.GroupMemberSupervisor,
        start: {Kaffe.GroupMemberSupervisor, :start_link, []},
        type: :supervisor
      },
      Avrora
    ]
    opts = [strategy: :one_for_one, name: Hello.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
