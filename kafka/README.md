kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 1 --replication-factor 1 --topic testtopic
kafka-topics.sh --list --bootstrap-server localhost:9092
kafka-console-producer.sh --topic testtopic --bootstrap-server localhost:9092
kafka-console-consumer.sh --topic testtopic --bootstrap-server localhost:9092
kafka-console-consumer.sh --topic testtopic --bootstrap-server localhost:9092 --from-beginning

kcat -b localhost:9092 -t testtopic -L

Kafka tombstone
idempotent

Kafka Streams?
ksqldb?

schema-registry-start config/schema-registry.properties
ksql-server-start config/ksql-server.properties
